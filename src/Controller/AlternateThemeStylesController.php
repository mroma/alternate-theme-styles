<?php

namespace Drupal\alternate_theme_styles\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Alternate theme styles routes.
 */
class AlternateThemeStylesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
