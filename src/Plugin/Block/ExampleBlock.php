<?php

namespace Drupal\alternate_theme_styles\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "alternate_theme_styles_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Alternate theme styles")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
