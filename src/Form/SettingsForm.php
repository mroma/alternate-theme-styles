<?php

namespace Drupal\alternate_theme_styles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure Alternate theme styles settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alternate_theme_styles_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['alternate_theme_styles.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //get Role list
    $roles = Role::loadMultiple();
    $roles_list = array();
    foreach ($roles as $key => $role){
      $roles_list[$role->id()] = $key;
    }

    //get file list
    $files_route = DRUPAL_ROOT."/".$this->config('alternate_theme_styles.settings')->get('theme_route');
    $files = scandir($files_route);
    $file_list = array();

    if($files){
      foreach($files as $file) {
        if (is_file($files_route ."/". $file)) {
          $file_list[$file] = $file;
        }
      }
    }


    $form['theme_route'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom CSS folder'),
      '#default_value' => $this->config('alternate_theme_styles.settings')->get('theme_route'),
    ];
    if ($files){
      $form['theme_route']['#description'] = $files_route;
    }
    $form['css_to_replace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS to replace'),
      '#default_value' => $this->config('alternate_theme_styles.settings')->get('css_to_replace'),
    ];
    $form['role_css'] = [
      '#type'=> 'webform_custom_composite',
      '#title' => 'Custom composite',
      '#element' => [
        'rol' => [
          '#type' => 'select',
          '#title' => 'Select role',
          '#options' => [$roles_list],
        ],
        'css' => [
          '#type' => 'select',
          '#title' => 'Select css',
          '#options' => [$file_list],
        ],
      ],
      '#default_value' => $this->config('alternate_theme_styles.settings')->get('role_css'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    if ($form_state->getValue('example') != 'example') {
//      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
//    }
//    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('alternate_theme_styles.settings')
      ->set('theme_route', $form_state->getValue('theme_route'))
      ->set('role_css', $form_state->getValue('role_css'))
      ->set('css_to_replace', $form_state->getValue('css_to_replace'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
